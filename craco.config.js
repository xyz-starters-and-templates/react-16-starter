/* craco.config.js */
const CracoLinariaPlugin = require('craco-linaria');

module.exports = {
  babel: {
    presets: ['@babel/preset-env', '@babel/preset-react', '@linaria'],
    plugins: [],
    loaderOptions: {
      /* Any babel-loader configuration options: https://github.com/babel/babel-loader. */
    },
    loaderOptions: (babelLoaderOptions, { env, paths }) => {
      return babelLoaderOptions;
    },
  },
  plugins: [
    {
      plugin: CracoLinariaPlugin,
      options: {
        // Linaria options
      },
    },
  ],
};
