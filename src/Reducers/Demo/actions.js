import { SET_DEMO_REDUCER } from './../type';

export const updateTextAction = () => {
  let text = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (let i = 0; i < 6; i++) text += characters.charAt(Math.floor(Math.random() * characters.length));
  return ({
    target: 'text',
    type: SET_DEMO_REDUCER,
    data: text,
  });
};
