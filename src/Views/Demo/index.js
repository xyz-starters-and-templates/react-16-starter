import React, { memo } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from "react-redux";

import { updateTextAction } from "../../Reducers/Demo/actions";
import { DemoTitle } from '../../Components/Styled';

const Demo = () => {
  const { t } = useTranslation();
  const { ref } = useParams();
  const dispatch = useDispatch();

  const demoText = useSelector(state => state.demo.text);
  
  const updateDemo = () => {
    const res = updateTextAction();
    dispatch(res);
  };

  return (
    <div className="home">
      <DemoTitle>
        {`${t('title')} ${demoText}`}
      </DemoTitle>
      <br />
      <button onClick={updateDemo}>
        {`Ref is ${ref || 'N/A'}`}
      </button>
    </div>
  );
};

export default memo(Demo);
