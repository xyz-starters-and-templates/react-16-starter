import React, { memo } from "react";
import { DemoTitle } from '../../Components/Styled';

const Home = () => {

  return (
    <div className="home">
      <DemoTitle>
        {'home'}
      </DemoTitle>
    </div>
  );
};

export default memo(Home);
