import axios from 'axios'
import { baseAPI } from './../Config';

/* eslint-disable */
const token = 'isaac_testing_token';
const service = axios.create({
  baseURL: baseAPI,
  timeout: 30000,
});

service.interceptors.request.use(
  config => {
    if (typeof(token) !== 'undefined') {
      config.headers['Authorization'] = token;
    } return config;
  },
  error => Promise.reject(error),
);

service.interceptors.response.use(
  response => {
    const res = response.data;
    if (res.status !== 'success') {
      alert(res.message);
      if (res.status === 'expired') location.reload();
      return Promise.reject('error');
    } else {
      if (res.message !== '') alert(res.message);
      return response.data;
    }
  },
  error => {
    const err = error.response;
    console.log(err);
    alert('Connection Problems');
    return Promise.reject(error)
  }
);

export default service;
