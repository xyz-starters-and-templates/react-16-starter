import service from './../Request';

export function demoService(x, y) {
  return service({
    url: '/xyz',
    method: 'post',
    data: { x, y }
  });
}
