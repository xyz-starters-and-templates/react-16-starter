import { styled } from '@linaria/react';

export const DemoTitle = styled.h1`
  font-size: 30px;
`;