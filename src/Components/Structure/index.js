import React from "react";
import Header from "./header";

const Structure = ({ children }) => {
  return (
    <div className="structure">
      <Header />
      {children}
    </div>
  );
};

export default Structure;
